"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _helmet = _interopRequireDefault(require("helmet"));

var _startup = _interopRequireDefault(require("./startup"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express.default)();
app.use((0, _helmet.default)());
app.set('view engine', 'pug');
(0, _startup.default)(app);
var _default = app;
exports.default = _default;