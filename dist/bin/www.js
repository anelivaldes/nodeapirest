"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("@babel/polyfill");

var _app = _interopRequireDefault(require("./../app"));

var _config = _interopRequireDefault(require("config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var port = _config.default.get('port');

var server = _app.default.listen(port, function () {
  console.log("Listening on port: ".concat(port, "..."));
});

var _default = server;
exports.default = _default;