"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var bcrypt = require('bcryptjs');

var _require = require('../models/user'),
    User = _require.User;

var Joi = require('joi');

module.exports = {
  login: function () {
    var _login = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(req, res) {
      var _validate, error, user, validPassword, token;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _validate = validate(req.body), error = _validate.error;

              if (!error) {
                _context.next = 3;
                break;
              }

              return _context.abrupt("return", res.status(400).send(error.details[0].message));

            case 3:
              _context.next = 5;
              return User.findOne({
                email: req.body.email
              });

            case 5:
              user = _context.sent;

              if (user) {
                _context.next = 8;
                break;
              }

              return _context.abrupt("return", res.status(400).send('Invalid email or password.'));

            case 8:
              _context.next = 10;
              return bcrypt.compare(req.body.password, user.password);

            case 10:
              validPassword = _context.sent;

              if (validPassword) {
                _context.next = 13;
                break;
              }

              return _context.abrupt("return", res.status(400).send('Invalid email or password.'));

            case 13:
              token = user.generateAuthToken();
              res.headers('x-auth-token', token).send(true);

            case 15:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function login(_x, _x2) {
      return _login.apply(this, arguments);
    }

    return login;
  }()
};

var validate = function validate(req) {
  var schema = {
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required()
  };
  return Joi.validate(req, schema);
};