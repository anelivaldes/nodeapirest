"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models/booking'),
    Booking = _require.Booking,
    validate = _require.validate;

var _require2 = require('../models/user'),
    User = _require2.User;

module.exports = {
  validateBody: function validateBody(req, res, next) {
    var _validate = validate(req.body),
        error = _validate.error;

    if (error) {
      return res.status(400).send(error.details[0].message);
    }

    next();
  },
  getAll: function () {
    var _getAll = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2() {
      var bookings, results;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return Booking.find().select(['-_id']);

            case 2:
              bookings = _context2.sent;
              _context2.next = 5;
              return Promise.all(bookings.map(
              /*#__PURE__*/
              function () {
                var _ref = _asyncToGenerator(
                /*#__PURE__*/
                regeneratorRuntime.mark(function _callee(b) {
                  var email, user;
                  return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          email = b.email;
                          _context.next = 3;
                          return User.findOne({
                            email: email
                          });

                        case 3:
                          user = _context.sent;
                          return _context.abrupt("return", _objectSpread({}, b._doc, {
                            user: user
                          }));

                        case 5:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee, this);
                }));

                return function (_x) {
                  return _ref.apply(this, arguments);
                };
              }()));

            case 5:
              results = _context2.sent;
              return _context2.abrupt("return", results);

            case 7:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function getAll() {
      return _getAll.apply(this, arguments);
    }

    return getAll;
  }(),
  addBooking: function () {
    var _addBooking = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3(booking) {
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              booking = new Booking(booking);
              _context3.next = 3;
              return booking.save();

            case 3:
              return _context3.abrupt("return", _context3.sent);

            case 4:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function addBooking(_x2) {
      return _addBooking.apply(this, arguments);
    }

    return addBooking;
  }()
};