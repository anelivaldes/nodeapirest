"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models/customer'),
    Customer = _require.Customer,
    validate = _require.validate;

module.exports = {
  validateBody: function validateBody(req, res, next) {
    var _validate = validate(req.body),
        error = _validate.error;

    if (error) {
      return res.status(400).send(error.details[0].message);
    }

    next();
  },
  getAll: function () {
    var _getAll = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return Customer.find().sort('-_id').select(['name', 'isGold', 'phone', '_id']);

            case 2:
              return _context.abrupt("return", _context.sent);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function getAll() {
      return _getAll.apply(this, arguments);
    }

    return getAll;
  }(),
  addCustomer: function () {
    var _addCustomer = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2(_ref) {
      var name, isGold, phone, customer;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              name = _ref.name, isGold = _ref.isGold, phone = _ref.phone;
              _context2.next = 3;
              return Customer.findOne({
                name: name
              });

            case 3:
              customer = _context2.sent;

              if (!customer) {
                _context2.next = 6;
                break;
              }

              return _context2.abrupt("return", false);

            case 6:
              customer = new Customer({
                name: name,
                isGold: isGold,
                phone: phone
              });
              _context2.next = 9;
              return customer.save();

            case 9:
              return _context2.abrupt("return", _context2.sent);

            case 10:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function addCustomer(_x) {
      return _addCustomer.apply(this, arguments);
    }

    return addCustomer;
  }(),
  getCustomer: function () {
    var _getCustomer = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3(req, res) {
      var customer;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return Customer.findById(req.params.id).select(['name', 'phone', '-_id', '__v']);

            case 2:
              customer = _context3.sent;

              if (customer) {
                _context3.next = 5;
                break;
              }

              return _context3.abrupt("return", res.status(404).send('The customer with the given ID was not found.'));

            case 5:
              res.send(customer);

            case 6:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function getCustomer(_x2, _x3) {
      return _getCustomer.apply(this, arguments);
    }

    return getCustomer;
  }(),
  updateCustomer: function () {
    var _updateCustomer = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee4(req, res) {
      var customer;
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return Customer.findByIdAndUpdate(req.params.id, {
                name: req.body.name,
                isGold: req.body.isGold,
                phone: req.body.phone
              }, {
                new: true
              });

            case 2:
              customer = _context4.sent;

              if (customer) {
                _context4.next = 5;
                break;
              }

              return _context4.abrupt("return", res.status(404).send('The customer with the given ID was not found.'));

            case 5:
              res.send(customer);

            case 6:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, this);
    }));

    function updateCustomer(_x4, _x5) {
      return _updateCustomer.apply(this, arguments);
    }

    return updateCustomer;
  }(),
  deleteCustomer: function () {
    var _deleteCustomer = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee5(req, res) {
      var customer;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return Customer.findByIdAndRemove(req.params.id);

            case 2:
              customer = _context5.sent;

              if (customer) {
                _context5.next = 5;
                break;
              }

              return _context5.abrupt("return", res.status(404).send('The customer with the given ID was not found.'));

            case 5:
              res.send(customer);

            case 6:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, this);
    }));

    function deleteCustomer(_x6, _x7) {
      return _deleteCustomer.apply(this, arguments);
    }

    return deleteCustomer;
  }()
};