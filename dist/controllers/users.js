"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _require = require('../models/user'),
    User = _require.User,
    validate = _require.validate;

var bcrypt = require('bcryptjs');

var _ = require('lodash');

module.exports = {
  getAll: function () {
    var _getAll = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(req, res) {
      var users;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return User.find().select(['name', '-_id']);

            case 2:
              users = _context.sent;
              res.status(200).send(users);

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function getAll(_x, _x2) {
      return _getAll.apply(this, arguments);
    }

    return getAll;
  }(),
  profile: function () {
    var _profile = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2(req, res) {
      var user;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return User.findById(req.user._id).select('-password');

            case 2:
              user = _context2.sent;
              res.send(user);

            case 4:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function profile(_x3, _x4) {
      return _profile.apply(this, arguments);
    }

    return profile;
  }(),
  addUser: function () {
    var _addUser = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3(req, res) {
      var _validate, error, user, salt, token;

      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _validate = validate(req.body), error = _validate.error;

              if (!error) {
                _context3.next = 3;
                break;
              }

              return _context3.abrupt("return", res.status(400).send(error.details[0].message));

            case 3:
              _context3.next = 5;
              return User.findOne({
                email: req.body.email
              });

            case 5:
              user = _context3.sent;

              if (!user) {
                _context3.next = 8;
                break;
              }

              return _context3.abrupt("return", res.status(400).send('User already registered.'));

            case 8:
              user = new User(_.pick(req.body, ['name', 'email', 'password', 'isAdmin']));
              _context3.next = 11;
              return bcrypt.genSalt(10);

            case 11:
              salt = _context3.sent;
              _context3.next = 14;
              return bcrypt.hash(user.password, salt);

            case 14:
              user.password = _context3.sent;
              _context3.next = 17;
              return user.save();

            case 17:
              token = user.generateAuthToken();
              res.header('x-auth-token', token).send(_.pick(user, ['_id', 'name', 'email']));

            case 19:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function addUser(_x5, _x6) {
      return _addUser.apply(this, arguments);
    }

    return addUser;
  }()
};