"use strict";

var Joi = require('joi');

Joi.objectId = require('joi-objectid')(Joi);

module.exports = function (req, res, next) {
  var schema = {
    id: Joi.objectId()
  };

  var _Joi$validate = Joi.validate(req.params, schema),
      error = _Joi$validate.error;

  if (error) {
    res.status(400).send(error);
  } else {
    next();
  }
};