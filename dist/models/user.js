"use strict";

var config = require('config');

var jwt = require('jsonwebtoken');

var Joi = require('joi');

var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 1024
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
});

userSchema.methods.generateAuthToken = function () {
  var token = jwt.sign({
    _id: this._id,
    isAdmin: this.isAdmin
  }, config.get('jwtPrivateKey'));
  return token;
};

var User = mongoose.model('User', userSchema);

var validateUser = function validateUser(user) {
  var schema = {
    name: Joi.string().min(5).max(50).required(),
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required(),
    isAdmin: Joi.boolean()
  };
  return Joi.validate(user, schema);
};

exports.User = User;
exports.validate = validateUser;