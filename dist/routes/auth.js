"use strict";

var Ctlr = require('./../controllers/auth');

var express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }]*/


var router = express.Router();
router.post('/', Ctlr.login);
module.exports = router;