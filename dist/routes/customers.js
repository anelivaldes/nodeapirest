"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }]*/


var router = express.Router();

var Ctlr = require('./../controllers/customers');

var ValidateId = require('./../middleware/validateId');

router.get('/',
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.t0 = res;
            _context.next = 3;
            return Ctlr.getAll();

          case 3:
            _context.t1 = _context.sent;

            _context.t0.send.call(_context.t0, _context.t1);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
router.post('/', Ctlr.validateBody,
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(req, res) {
    var result;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return Ctlr.addCustomer(req.body);

          case 2:
            result = _context2.sent;

            if (!result) {
              _context2.next = 7;
              break;
            }

            return _context2.abrupt("return", res.send(result));

          case 7:
            return _context2.abrupt("return", res.status(400).send('Customer already registered.'));

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());
router.put('/:id', ValidateId, Ctlr.validateBody, Ctlr.updateCustomer);
router.patch('/:id', ValidateId, Ctlr.validateBody, Ctlr.updateCustomer);
router.delete('/:id', ValidateId, Ctlr.deleteCustomer);
router.get('/:id', ValidateId, Ctlr.getCustomer);
module.exports = router;