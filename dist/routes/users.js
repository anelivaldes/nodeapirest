"use strict";

var auth = require('../middleware/auth');

var admin = require('../middleware/admin');

var Ctlr = require('./../controllers/users');

var express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }]*/


var router = express.Router();
router.get('/', [auth, admin], Ctlr.getAll);
router.get('/me', auth, Ctlr.profile);
router.post('/', Ctlr.addUser);
module.exports = router;