"use strict";

var config = require('config');

var mongoose = require('mongoose');

var db = config.get('db');

module.exports = function () {
  return mongoose.connect(db, {
    useNewUrlParser: true
  }).then(function () {
    console.log('Connected to MongoDB - ENV: ' + (process.env.NODE_ENV || 'default') + " - DB: ".concat(db));
  }).catch(function (err) {
    console.log('Could not connect to MongoDB');
    console.log(err);
    process.exit(1);
  });
};