"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _config = _interopRequireDefault(require("./config"));

var _logging = _interopRequireDefault(require("./logging"));

var _db = _interopRequireDefault(require("./db"));

var _routes = _interopRequireDefault(require("./routes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var start = function start(app) {
  (0, _config.default)();
  (0, _logging.default)();
  (0, _db.default)();
  (0, _routes.default)(app);
};

var _default = start;
exports.default = _default;