"use strict";

var winston = require('winston');

require('express-async-errors');

module.exports = function () {
  winston.exceptions.handle(new winston.transports.File({
    filename: 'uncaughtExceptions.log'
  }));
  process.on('unhandledRejection', function (ex) {
    console.log('unhandledRejection: ', ex);
    throw ex;
  });
  process.on('uncaughtException', function (err) {
    console.log("Caught exception: ".concat(err, "\n"));
  });
};