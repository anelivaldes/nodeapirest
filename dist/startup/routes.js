"use strict";

var express = require('express');

var users = require('./../routes/users');

var customers = require('./../routes/customers');

var auth = require('./../routes/auth');

var booking = require('./../routes/booking');

var error = require('./../middleware/error');

var logger = require('./../middleware/logger');

var pages = require('./../routes/pages');

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });
  app.use(express.json());
  app.use(express.urlencoded({
    extended: true
  }));
  app.use('/', logger);
  app.use(express.static('public'));
  app.use('/api/users', users);
  app.use('/api/customers', customers);
  app.use('/api/auth', auth);
  app.use('/api/booking', booking);
  app.use('/', pages);
  app.use(error);
};