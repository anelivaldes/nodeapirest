"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var server;

var request = require('supertest');

var _require = require('./../../models/customer'),
    Customer = _require.Customer;

describe('/api/genres', function () {
  beforeEach(function () {
    server = require('./../../bin/www');
  });
  afterEach(
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return server.close();

          case 2:
            _context.next = 4;
            return Customer.deleteMany();

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  })));
  describe('GET /', function () {
    it('should return all genres',
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2() {
      var res;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return Customer.collection.insertMany([{
                name: 'Cliente1',
                phone: '12345',
                isGold: true
              }, {
                name: 'Cliente2',
                phone: '5678',
                isGold: false
              }]);

            case 2:
              _context2.next = 4;
              return request(server).get('/api/customers');

            case 4:
              res = _context2.sent;
              expect(res.status).toBe(200);
              expect(res.body.length).toBe(2);
              expect(res.body.some(function (c) {
                return c.name === 'Cliente2';
              })).toBeTruthy();

            case 8:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    })));
  });
  describe('GET /:id', function () {
    it('should return a customer if valid id is passed',
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3() {
      var res;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return Customer.collection.insertMany([{
                name: 'Cliente1',
                phone: '12345',
                isGold: true
              }, {
                name: 'Cliente2',
                phone: '5678',
                isGold: false
              }]);

            case 2:
              _context3.next = 4;
              return request(server).get('/api/customers');

            case 4:
              res = _context3.sent;
              expect(res.status).toBe(200);
              expect(res.body.length).toBe(2);
              expect(res.body.some(function (c) {
                return c.name === 'Cliente2';
              })).toBeTruthy();

            case 8:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    })));
  });
});