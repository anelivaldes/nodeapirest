"use strict";

var renderTemplate = function renderTemplate(res, template, data) {
  res.render(template, data);
};

module.exports = renderTemplate;