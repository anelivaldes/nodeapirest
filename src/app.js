import express from 'express';
import helmet from 'helmet';
import startApp from './startup';

const app = express();
app.use(helmet());
app.set('view engine', 'pug');
startApp(app);

export default app;
