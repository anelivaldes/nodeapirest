
import '@babel/polyfill';
import app from './../app';
import config from 'config';
const port = config.get('port');

const server = app.listen(port, () => {
  console.log(`Listening on port: ${port}...`);
});

export default server;
