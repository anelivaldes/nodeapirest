const {Booking, validate} = require('../models/booking');
const {User} = require('../models/user');


module.exports = {
  validateBody: (req, res, next) => {
    const {error} = validate(req.body);
    if (error) {
      return res.status(400).send(error.details[0].message);
    }
    next();
  },
  getAll: async () => {
    const bookings = await Booking.find().select(['-_id']);
    const results = await Promise.all(bookings.map(async (b)=> {
      const {email} = b;
      const user = await User.findOne({email: email});
      return {...b._doc, user};
    }));
    return results;
  },
  addBooking: async (booking) => {
    booking = new Booking(booking);
    return await booking.save();
  },
};
