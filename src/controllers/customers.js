const {Customer, validate} = require('../models/customer');
module.exports = {
  validateBody: (req, res, next) => {
    const {error} = validate(req.body);
    if (error) {
      return res.status(400).send(error.details[0].message);
    }
    next();
  },
  getAll: async () => {
    return await Customer.find().sort('-_id')
        .select(['name', 'isGold', 'phone', '_id']);
  },
  addCustomer: async ({name, isGold, phone}) => {
    let customer = await Customer.findOne({name: name});
    if (customer) return false;

    customer = new Customer({
      name: name,
      isGold: isGold,
      phone: phone,
    });

    return await customer.save();
  },
  getCustomer: async (req, res) => {
    const customer = await Customer.findById(req.params.id)
        .select(['name', 'phone', '-_id', '__v']);
    if (!customer) {
      return res.status(404)
          .send('The customer with the given ID was not found.');
    }
    res.send(customer);
  },
  updateCustomer: async (req, res) => {
    const customer = await Customer.findByIdAndUpdate(req.params.id,
        {
          name: req.body.name,
          isGold: req.body.isGold,
          phone: req.body.phone,
        }, {new: true});

    if (!customer) {
      return res.status(404)
          .send('The customer with the given ID was not found.');
    }
    res.send(customer);
  },
  deleteCustomer: async (req, res) => {
    const customer = await Customer.findByIdAndRemove(req.params.id);
    if (!customer) {
      return res.status(404)
          .send('The customer with the given ID was not found.');
    }
    res.send(customer);
  },
};
