module.exports = function(handler) {
  return async (req, res, next) => {
    try {
      await handler(req, res);
    } catch (ex) {
      next(ex);
    }
  };
};
/**
 * this is a solution to wrapper or async functions on routers,
 *  but is repetitive, it will be no necessary
 * if we use express-async-errors require at
 * the beggining and capture with error middlerware using after
 * all routes
 * ;
 */
