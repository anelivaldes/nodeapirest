const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

module.exports = function(req, res, next) {
  const schema = {id: Joi.objectId()};
  const {error} = Joi.validate(req.params, schema);
  if (error) {
    res.status(400).send(error);
  } else {
    next();
  }
};
