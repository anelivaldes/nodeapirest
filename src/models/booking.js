const Joi = require('joi');
const mongoose = require('mongoose');

const bookingSchema = new mongoose.Schema({
  type: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  bookingDate: {
    type: Date,
    required: true,
  },
  peoples: {
    type: Number,
    min: 1,
    max: 10,
  },
  comments: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
  },
});

const Booking = mongoose.model('Booking', bookingSchema);

const validateBooking = (booking) => {
  const schema = {
    type: Joi.any().valid('Tour', 'Transfer').required(),
    date: Joi.date(),
    bookingDate: Joi.date().required(),
    peoples: Joi.number().min(1).max(11),
    comments: Joi.string(),
    email: Joi.string().min(5).max(255).required().email(),
  };
  return Joi.validate(booking, schema);
};

exports.Booking = Booking;
exports.validate = validateBooking;
