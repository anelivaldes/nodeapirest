const Ctlr = require('./../controllers/auth');
const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }]*/
const router = express.Router();

router.post('/', Ctlr.login);

module.exports = router;
