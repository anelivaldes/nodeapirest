const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const Ctlr = require('../controllers/booking');
const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }]*/
const router = express.Router();


router.get('/', [auth, admin], async (req, res)=>{
  const bookings = await Ctlr.getAll();
  res.status(200).send(bookings);
});

router.post('/', Ctlr.validateBody, async (req, res)=>{
  const result = await Ctlr.addBooking(req.body);
  return res.send(result);
});

module.exports = router;
