const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }]*/
const router = express.Router();
const Ctlr = require('./../controllers/customers');
const ValidateId = require('./../middleware/validateId');

router.get('/', async (req, res)=>{
  res.send(await Ctlr.getAll());
});

router.post('/', Ctlr.validateBody, async (req, res)=>{
  const result = await Ctlr.addCustomer(req.body);
  if (result) {
    return res.send(result);
  } else {
    return res.status(400).send('Customer already registered.');
  }
});

router.put('/:id', ValidateId, Ctlr.validateBody, Ctlr.updateCustomer);

router.patch('/:id', ValidateId, Ctlr.validateBody, Ctlr.updateCustomer);

router.delete('/:id', ValidateId, Ctlr.deleteCustomer);

router.get('/:id', ValidateId, Ctlr.getCustomer);

module.exports = router;
