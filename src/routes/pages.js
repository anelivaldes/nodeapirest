const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }]*/
const router = express.Router();
const render = require('./../utils');
const customersCtrl = require('./../controllers/customers');


router.get('/', async (req, res) => {
  const customers = await customersCtrl.getAll();
  render(res, 'index', {message: 'Hi Ane', list: customers.map((c) => c._id)});
});

module.exports = router;
