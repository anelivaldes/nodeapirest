const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const Ctlr = require('./../controllers/users');
const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }]*/
const router = express.Router();


router.get('/', [auth, admin], Ctlr.getAll);

router.get('/me', auth, Ctlr.profile);

router.post('/', Ctlr.addUser);

module.exports = router;
