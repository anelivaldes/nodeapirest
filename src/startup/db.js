const config = require('config');
const mongoose = require('mongoose');
const db = config.get('db');

module.exports = function() {
  return mongoose.connect(db, {useNewUrlParser: true})
      .then(() => {
        console.log('Connected to MongoDB - ENV: ' +
        (process.env.NODE_ENV || 'default') +
        ` - DB: ${db}`);
      })
      .catch((err) => {
        console.log('Could not connect to MongoDB');
        console.log(err);
        process.exit(1);
      });
};
