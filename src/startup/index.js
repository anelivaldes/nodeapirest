import configEnv from './config';
import initLogs from './logging';
import initDataBase from './db';
import configRoutes from './routes';


const start = (app) => {
  configEnv();
  initLogs();
  initDataBase();
  configRoutes(app);
};

export default start;
