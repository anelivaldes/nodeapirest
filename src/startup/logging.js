const winston = require('winston');
require('express-async-errors');

module.exports = function() {
  winston.exceptions.handle(
      new winston.transports.File({filename: 'uncaughtExceptions.log'}));

  process.on('unhandledRejection', (ex) => {
    console.log('unhandledRejection: ', ex);
    throw ex;
  });
  process.on('uncaughtException', (err) => {
    console.log(`Caught exception: ${err}\n`);
  });
};
