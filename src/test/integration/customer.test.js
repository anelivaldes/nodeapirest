let server;
const request = require('supertest');
const {Customer} = require('./../../models/customer');

describe('/api/genres', () => {
  beforeEach(() => {
    server = require('./../../bin/www');
  });
  afterEach(async () => {
    await server.close();
    // To clean the db
    await Customer.deleteMany();
  });
  describe('GET /', () => {
    it('should return all genres', async () => {
      await Customer.collection.insertMany([
        {
          name: 'Cliente1',
          phone: '12345',
          isGold: true,
        },
        {
          name: 'Cliente2',
          phone: '5678',
          isGold: false,
        },
      ]);
      const res = await request(server).get('/api/customers');
      expect(res.status).toBe(200);
      expect(res.body.length).toBe(2);
      expect(res.body.some((c) => c.name === 'Cliente2')).toBeTruthy();
    });
  });
  describe('GET /:id', () => {
    it('should return a customer if valid id is passed', async () => {
      await Customer.collection.insertMany([
        {
          name: 'Cliente1',
          phone: '12345',
          isGold: true,
        },
        {
          name: 'Cliente2',
          phone: '5678',
          isGold: false,
        },
      ]);
      const res = await request(server).get('/api/customers');
      expect(res.status).toBe(200);
      expect(res.body.length).toBe(2);
      expect(res.body.some((c) => c.name === 'Cliente2')).toBeTruthy();
    });
  });
});
