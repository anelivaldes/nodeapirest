const renderTemplate = (res, template, data) => {
  res.render(template,
      data);
};
module.exports= renderTemplate;
